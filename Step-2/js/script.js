var Menubar = {};

Menubar.active = false;

Menubar.open = function(el) {
  var menubar = $(el).closest('.menubar')
  ,   item = $(el).closest('li')
  ,   menu = item.find('.menu').first()
  ;
  Menubar.active = true;
  item.addClass('is-selected')
  item.siblings().removeClass('is-selected');
};

Menubar.close = function() {
  $('.menubar-item.is-selected').removeClass('is-selected');
  Menubar.active = false;
};

$('.menubar > li').on('click', function(e) {
  if (!Menubar.active) {
    Menubar.open(this);
    e.stopPropagation(); // Keep document.click from firing
  }
});

$('.menubar > li').on('mouseenter', function() {
  if (Menubar.active) { Menubar.open(this); }
});

$(document).on('click', function() {
  Menubar.close();
});

$(document).ready(function () {
  $('#mydiv').click(function(){
    alert("My name is Tony")
  })
})