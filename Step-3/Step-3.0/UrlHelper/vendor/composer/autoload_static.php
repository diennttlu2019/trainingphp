<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit9c320b501007caf78ab786916a6554d7
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PackageUrlHelper\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PackageUrlHelper\\' => 
        array (
            0 => __DIR__ . '/..' . '/dien/url_helper/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit9c320b501007caf78ab786916a6554d7::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit9c320b501007caf78ab786916a6554d7::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
