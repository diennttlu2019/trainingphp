<?php

/*
 * Author: Mizanur Rahman <mizanur.rahman@gmail.com>
 * 
 */
require_once 'LinkedList.php';

interface QueueInterface {

    public function enqueue(string $item);

    public function dequeue();

    public function peek();

    public function isEmpty();
}

class Queue implements QueueInterface {

	private $limit;
	private $queue;

	public function __construct(int $limit = 20) {
		$this->limit = $limit;
		$this->queue = new LinkedList();
	}

	public function dequeue(): string {

		if ($this->isEmpty()) {
			throw new UnderflowException('Queue is empty');
		} else {
			$lastItem = $this->peek();
			$this->queue->deleteFirst();
			return $lastItem;
		}
	}

	public function enqueue(string $newItem) {

		if ($this->queue->getSize() < $this->limit) {
			$this->queue->insert($newItem);
		} else {
			throw new OverflowException('Queue is full');
		}
	}

	public function peek(): string {
		return $this->queue->getNthNode(1)->data; // lấy ra data của Node đầu tiên
	}

	public function isEmpty(): bool {
		return $this->queue->getSize() == 0;
	}

}
