<?php 
require_once 'Queue.php';

$arr_number = array(1, 2, 3, 7, 10, 9);
$queue = new Queue();

for ($i=0; $i < count($arr_number); $i++) { 
	$queue->enqueue($arr_number[$i]);
}

echo 'Dequeue: '.$queue->dequeue();
