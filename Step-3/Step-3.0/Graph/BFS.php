
<?php
require_once 'Queue/Queue.php';

function BFS(array &$graph, int $start, int $destination, array $visited){
    $queue = new Queue();
    $path = new Queue();

    $queue->enqueue($start);
    $visited[$start] = 1;

    while (!$queue->isEmpty()) {
        $node = $queue->dequeue();
        echo $node." ";
        if ($destination == $node) {
        	return true;
        }else{
        	$path->enqueue($node);
	        foreach ($graph[$node] as $key => $vertex) {
	            if (!$visited[$key] && $vertex == 1) { // $vertex == 1 -> đỉnh kề
	                $visited[$key] = 1;
	                $queue->enqueue($key);
	            }
	        }
        } 
    }
    return false;
}

$graph = [];
$visited = [];
$vertexCount = 6;

for ($i = 1; $i <= $vertexCount; $i++) {
    $graph[$i] = array_fill(1, $vertexCount, 0);
    $visited[$i] = 0;
}

$graph[1][2] = $graph[2][1] = 1;
$graph[1][5] = $graph[5][1] = 1;
$graph[5][2] = $graph[2][5] = 1;
$graph[5][4] = $graph[4][5] = 1;
$graph[4][3] = $graph[3][4] = 1;
$graph[3][2] = $graph[2][3] = 1;
$graph[6][4] = $graph[4][6] = 1;


