<?php require_once 'BFS.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>BFS</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<style type="text/css">
		*{
			padding: 0;
			margin: 0;
		}
		.row{
			margin-top: 20px;
		}
	</style>
	<script type="text/javascript">
		function validate() {
			var des = document.getElementById("destination").value;
			var start = document.getElementById("start").value;
			if (des == "" || start == "") {
				return false;
			}else{
				return true;
			}
		}
	</script>
	
</head>
<body>
<?php
	echo "<span>Input: </span><br>";
	echo "Start: root = 1 <br>";
	echo "<span>Output: </span>"; 
	if (isset($_POST['submitDestination'])) {
		$start = $_POST['start'];
		$destination = $_POST['destination'];
		echo "<br>Thứ tự duyệt: ";

		if (BFS($graph, $start, $destination, $visited)) {
			echo "<br>Có đỉnh ".$destination." trong đồ thị";
		}else{
			echo "Không có đỉnh ".$destination." trong đồ thị";
		}
	}
 ?>
<div class="container-fluid">
	<div class="row">
		<form action="index.php" method="post" onsubmit="return validate();">
			Đỉnh bắt đầu:
			<input id="start" type="text" name="start">
			Đỉnh kết thúc:
			<input id="destination" type="text" name="destination">
			<input type="submit" name="submitDestination" value="Tìm">
		</form>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<?php 
				echo "Đồ thị: <br>";   
				for ($i = 1; $i <= count($graph); $i++) { 
				    echo $i.' => [';
				    foreach ($graph[$i] as $key => $vertex) {
				            echo $vertex.', ';
				    }
				     echo ']<br>';
				}
			?>
		</div>
		<div class="col-md-6">
			<img src="graph-image.png" width="auto" height="auto">
		</div>
	</div>
</div>

</body>
</html>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>